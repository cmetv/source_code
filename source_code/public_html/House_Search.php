<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/mystyle.css">
  <script
  src="http://maps.googleapis.com/maps/api/js">
  
</script>
  
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGBun6yur5ZR_6vURWVbXSivfeK8d65fg"></script>

<script>
  var myCenter=new google.maps.LatLng(51.508742,-0.120850);

  function initialize()
  {

    var content = '<iframe width="220" height="115"'+
    'src="https://www.youtube.com/embed/XGSy3_Czz8k" allowfullscreen="allowfullscreen">'+
    '</iframe>';

    var mapProp = {
      center:myCenter,
      zoom:5,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    };

    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

    var marker=new google.maps.Marker({
      position:myCenter,

    });

    marker.setMap(map);

    var infowindow = new google.maps.InfoWindow({
      content:content
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });
  }

  google.maps.event.addDomListener(window, 'load', initialize);
  // Resize stuff...
  google.maps.event.addDomListener(window, "resize", function() {
   var center = map.getCenter();
   google.maps.event.trigger(map, "resize");
   map.setCenter(center); 
 });
</script>
</head>

<?php include '../src/get_data.php';?>
<!--获取php值 -->
<?php
//	echo $_GET['type'];
	get_data_fromdb($_GET['type'],$_GET['lower_limit'],$_GET['upper_limit'],$_GET['r_number'],$_GET['br_number']);
 
?>
<!--获取php值 -->



<body>
  <div class="web_head .container-fluid">
    <div class="row">
      <h3>加中置业地产频道</h3>
      <div class="dropdown user_login">
        <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Login/登录
          <span class="caret"></span></button>
          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
            <li role="presentation" class="dropdown-header">Username/用户名</li>
            <li role="presentation"><input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email"></li>
            <!--<li role="presentation" class="divider"></li>-->
            <li role="presentation" class="dropdown-header">Password/密码</li>
            <li role="presentation"><input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"></li>
            <li role="presentation"><button type="submit" class="btn btn-default">Submit</button></li>
          </ul>
        </div>
        <div class="lang">
          <a href="#">中文/English</a>
        </div>
      </div>
    </div>

    <div class="currentSearch">
      您当前的选择是：kennedy & 16 th Ave, 独立屋，$50万 - $100万，1500-2000平方尺(150-200平方米)
    </div>



    <div id="inputGroup">
      <div class="container-fluid">
        <form role="form">
          <div class="form-group col-lg-2 mb padLefRight">
            <select class="form-control" id="sel1">
              <option value="" disabled selected>▼社区所在</option>
              <option>万锦</option>
              <option>列治文山</option>
              <option>多伦多</option>
              <option>宾顿</option>
              <option>密西沙加</option>
              <option>无偏好</option>
            </select>
          </div>
          <div class="form-group col-lg-2 mb padLefRight">
            <select class="form-control" id="sel1"">
              <option value="" disabled selected>▼房屋种类</option>
              <option>自住</option>
              <option>停车位</option>
              <option>农田</option>
              <option>空地</option>
              <option>无偏好</option>
            </select>
          </div>
          <div class="form-group col-lg-2 mb padLefRight">
            <select class="form-control" id="sel1">
              <option value="" disabled selected>▼价格范围</option>
              <option>1000-2000</option>
              <option>2000-3000</option>
              <option>3000-4000</option>
              <option>无偏好</option>
            </select>
          </div>
          <div class="form-group col-lg-2 mb padLefRight">
            <select class="form-control" id="sel1">
              <option value="" disabled selected>▼房间数量</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>无偏好</option>
            </select>
          </div>
          <div class="form-group col-lg-2 mb padLefRight">
            <select class="form-control" id="sel1">
              <option value="" disabled selected>▼卫生间数量</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>无偏好</option>
            </select>
          </div>
          <button type="submit" class="btn btn-default col-lg-1 padLefRight sort">按价格排列</button>
          <div class="form-group col-lg-2 mb padLefRight">
            <select class="form-control" id="sel1">
              <option value="" disabled selected>▼按经纪搜索</option>
              <option>赵经纪</option>
              <option>钱经纪</option>
              <option>孙经纪</option>
              <option>李经纪</option>
              <option>无偏好</option>
            </select>
          </div>
          <div class="form-group col-lg-2 padLefRight">
            <select class="form-control" id="sel1">
              <option value="" disabled selected>▼出租/卖</option>
              <option>出租</option>
              <option>卖</option>
              <option>无偏好</option>
            </select>
          </div>
          <div class="form-group col-lg-2 padLefRight">
            <div class="dateTimePicker">
              <input type="text" value="" class="form-control" id="datetimepicker" placeholder="▼放盘时间" />  
            </div>
          </div>
          <div class="form-group col-lg-2 padLefRight">
            <select class="form-control" id="sel1">
              <option value="" disabled selected>▼更多选择</option>
              <option>选择1</option>
              <option>选择2</option>
              <option>选择3</option>
              <option>选择4</option>
              <option>无偏好</option>
            </select>
          </div>
          <div class="form-group col-lg-2 padLefRight">
           <div class="form-control">
            <label><input type="checkbox" value=""><span class="openToday">今日开放</span></label>
          </div>         
        </div>

        <button type="submit" class="btn btn-default col-lg-1 padLefRight sort">按面积排列</button>    
      </form>
    </div> <!--container -->
  </div>

  <div class="mainBlock container-fluid">
    <div class="row">
      <div class="searchResultCount">
        1个搜索结果
      </div>
      <div class="col-md-4 pre-scrollable">
        <div class="info-box">
          <div class="address">
            地址：13 Christian Ritter Dr, Markham, Ontario, Canada, L6C0W3
          </div>
          <img src="img/realtor_info.PNG" class="realtor_img">
          <div class="houseInfo">
            <div>价格$699,800</div>
            <div>Listing#:N3489714</div>
            <div>面积：2000sqft(185m2)</div>
            <div>房间：三室两厅三卫</div>
            <div>车库：两车位</div>
            <div>经济:Bin Ji</div>
            <a href="#"><img src="img/store.PNG"></a>
            <a href="#"><img src="img/forward.PNG"></a>
            <a href="#"><img src="img/call.PNG"></a>
          </div>
        </div>
        <div class="info-box">
          <div class="address">
            地址：13 Christian Ritter Dr, Markham, Ontario, Canada, L6C0W3
          </div>
          <img src="img/realtor_info.PNG" class="realtor_img">
          <div class="houseInfo">
            <div>价格$699,800</div>
            <div>Listing#:N3489714</div>
            <div>面积：2000sqft(185m2)</div>
            <div>房间：三室两厅三卫</div>
            <div>车库：两车位</div>
            <div>经济:Bin Ji</div>
            <a href="#"><img src="img/store.PNG"></a>
            <a href="#"><img src="img/forward.PNG"></a>
            <a href="#"><img src="img/call.PNG"></a>
          </div>
        </div>
      </div>
      <div id="googleMap" class="col-md-4">
      </div>
    </div>

  </div>

  <!--<img src="img/house_search.png">-->

</body>
<script src="js/jquery.js"></script>
<script src="js/jquery.datetimepicker.full.js"></script>
<script>

  $('#datetimepicker').datetimepicker();

</script>
</html>

